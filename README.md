# Lucas Store

Bem-vindo à Lucas Store - Sua loja online para produtos incríveis!

## Descrição do Projeto

A Lucas Store é uma aplicação web destinada a oferecer uma experiência de compra online fácil e conveniente. Aqui, você pode encontrar uma variedade de produtos de alta qualidade, eletrônicos e tecnologicos.

## Funcionalidades Principais

- Navegação intuitiva e amigável
- Catálogo diversificado de produtos
- Carrinho de compras ilustrativo


## Contato

Para dúvidas ou mais informações sobre a Lucas Store, entre em contato conosco:

- Email: lucasferreiraferraz@outlook.com
- Contato: 83 981783350
